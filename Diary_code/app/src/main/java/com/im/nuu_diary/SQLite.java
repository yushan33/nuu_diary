package com.im.nuu_diary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class SQLite extends SQLiteOpenHelper {
    SQLiteDatabase MyDb;
    final static String DATABASE_NAME = "diary";
    final static String TABLE_NAME="record";
    final static int DATABASE_VERSION = 1;
    final static String ID="id";
    final static String TITLE="title";
    final static String CONTENT="content";
    final static String DATETIME="datetime";
    final static String MOOD="mood";


    public SQLite(Context context ){
        super(context,DATABASE_NAME,null,DATABASE_VERSION );
        toNewTable(TABLE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        MyDb=db;

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void toNewTable(String name) {        //建立表格(預設儲存)(id, cap, warn, up, low,pid)
        String sql = "CREATE TABLE " + name + "(" + ID + " INTEGER primary key autoincrement,"
                + TITLE + " TEXT(30),"
                + MOOD + " INTEGER,"
                + CONTENT + " TEXT(30),"
                + DATETIME + " TEXT(30))";

        Log.e("sql", sql);
        SQLiteDatabase dbw = this.getWritableDatabase();
        try {
            dbw.execSQL(sql);
        } catch (Exception e) {
        }
        dbw.close();
    }


    public long toInsertData(String title,int mood,String content,String datatime){
        String tableName=TABLE_NAME;
        ContentValues cv=new ContentValues();
        SQLiteDatabase dbw=this.getWritableDatabase();

        cv.put(TITLE,title);
        cv.put(MOOD,mood);
        cv.put(CONTENT,content);
        cv.put(DATETIME,datatime);

        long id=dbw.insert(tableName,null,cv);

        dbw.close();

        return id;

    }

    public ArrayList<Record> isHaveData(){
        String tableName=TABLE_NAME;
        Cursor cursor =null;
        SQLiteDatabase dbr=this.getReadableDatabase();
        //游標指向該資料表
        cursor=dbr.query(tableName,null,null,null,null,null,null);

        ArrayList<Record> records=new ArrayList<>();

        //將所有資料轉成Item並添加進List
        while (cursor.moveToNext()) {
            records.add(getRecord(cursor));
        }
        //關閉游標
        //cursor.close();
        return records;
    }

    public Record getRecord(Cursor cursor){
        //準備回傳結果用的物件
        Record result = new Record();
        result.setId(cursor.getInt(0));
        result.setTitle(cursor.getString(1));
        result.setMood(cursor.getInt(2));
        result.setContent(cursor.getString(3));
        result.setDatetime(cursor.getString(4));

        String r="ID:"+result.getId()+"Title:"+result.getTitle()+"DateTime:"+result.getDatetime();
        Log.e("result",r);
        return result;

    }

    public boolean delete(int id){
        SQLiteDatabase dbw=this.getWritableDatabase();
        // 設定條件為編號，格式為「欄位名稱=資料」
        String where=ID+"="+id;
        // 刪除指定編號資料並回傳刪除是否成功
        return dbw.delete(TABLE_NAME, where , null) > 0;
    }

    // 取得指定編號的資料物件
    public Record get(long id) {
        SQLiteDatabase dbw=this.getWritableDatabase();

        // 準備回傳結果用的物件
        Record record = null;
        // 使用編號為查詢條件
        String where = ID + "=" + id;
        // 執行查詢
        Cursor result = dbw.query(TABLE_NAME, null, where, null, null, null, null, null);
        // 如果有查詢結果
        if (result.moveToFirst()) {
            // 讀取包裝一筆資料的物件
            record = getRecord(result);
        }
        // 關閉Cursor物件
        result.close();
        // 回傳結果
        return record;
    }

    // 修改參數指定的物件
    public boolean update(int id,String title,int mood,String content, String datetime) {
        SQLiteDatabase dbw=this.getWritableDatabase();
        // 建立準備修改資料的ContentValues物件
        ContentValues cv = new ContentValues();
        // 加入ContentValues物件包裝的修改資料
        // 第一個參數是欄位名稱， 第二個參數是欄位的資料
        cv.put(TITLE,title);
        cv.put(MOOD,mood);
        cv.put(CONTENT,content);
        cv.put(DATETIME,datetime);

        // 設定修改資料的條件為編號
        // 格式為「欄位名稱＝資料」
        String where = ID + "=" + id;
        // 執行修改資料並回傳修改的資料數量是否成功
        return dbw.update(TABLE_NAME, cv, where, null) > 0;
    }
}
