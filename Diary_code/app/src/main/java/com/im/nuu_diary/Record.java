package com.im.nuu_diary;

public class Record {
    private int id;
    private String title;
    private String content;
    private String datetime;
    private int mood;

    public Record(){
        content="";
    }

    public Record(int id,String title,int mood, String content,String datetime){
        this.id=id;
        this.title=title;
        this.mood=mood;
        this.content=content;
        this.datetime=datetime;
    }

    public void setId(int id){
        this.id=id;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public void setMood(int mood ){
        this.mood=mood;
    }

    public void setContent(String content){
        this.content=content;
    }

    public void setDatetime(String datetime){
        this.datetime=datetime;
    }

    public int getId(){
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public int getMood(){
        return this.mood;
    }

    public String getContent(){
        return this.content;
    }

    public String getDatetime(){
        return this.datetime;
    }


}
