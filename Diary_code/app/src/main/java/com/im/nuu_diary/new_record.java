package com.im.nuu_diary;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;

public class new_record extends AppCompatActivity {
    Toolbar toolbar;
    EditText edTitle,edContent;
    ImageButton ib1,ib2,ib3,ib4,ib5,ib6;
    Button bsend;
    //SQLite sqLite=new SQLite(new_record.this);
    private SQLite sqLite;
    private SQLiteDatabase db;
    int[] imgButton={R.id.imageButton,R.id.imageButton2,R.id.imageButton3,R.id.imageButton4,R.id.imageButton5,R.id.imageButton6};
    String[] st={"laugh","smile","angry","cry","worry","I want to dead"};
    View viewF,viewN;

    String title,content,datetime;
    int img=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_record);
        findview();
        toolbar=(Toolbar)findViewById(R.id.toolbar2);


        // 建立資料庫物件
        sqLite=new SQLite(new_record.this);
        db = sqLite.getWritableDatabase(); // 開啟資料庫
        sqLite.toNewTable("record");

        toolbar.setTitle("Insert");
        setSupportActionBar(toolbar);

        // Menu item click 的監聽事件一樣要設定在 setSupportActionBar 才有作用
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);

    }


    private Toolbar.OnMenuItemClickListener onMenuItemClickListener= new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            if(!edTitle.getText().toString().equals("") && img!=-1){
                title=edTitle.getText().toString();
                content=edContent.getText().toString();

                SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = sDateFormat.format(new java.util.Date());
                datetime=date;
                Log.e("time1111111111",datetime);

                sqLite.toInsertData(title,img,content,datetime);

                System.exit(0);

            }else{
                Toast.makeText(new_record.this,"could not null! ",Toast.LENGTH_LONG).show();
            }



            return true;
        }
    };

    public void ibClick(View v){


        for(int i=0;i<6;i++){
            if(v.getId()==imgButton[i]){
                viewF=viewN;
                img=i;
                viewN=v;
                v.getBackground().setAlpha(20);
            }
        }
        if(viewF!=null){
            viewF.getBackground().setAlpha(255);
        }


        Log.e("img2222",Integer.toString(img));
        Toast.makeText(new_record.this,"You choose "+st[img]+" ",Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 為了讓 Toolbar 的 Menu 有作用，這邊的程式不可以拿掉
        getMenuInflater().inflate(R.menu.insert, menu);
        return true;
    }

    public void findview(){
        edTitle=(EditText)findViewById(R.id.EDtitle);
        edContent=(EditText)findViewById(R.id.EDcontent);
        edContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        ib1=(ImageButton)findViewById(R.id.imageButton);
        ib2=(ImageButton)findViewById(R.id.imageButton2);
        ib3=(ImageButton)findViewById(R.id.imageButton3);
        ib4=(ImageButton)findViewById(R.id.imageButton4);
        ib5=(ImageButton)findViewById(R.id.imageButton5);
        ib6=(ImageButton)findViewById(R.id.imageButton6);

    }


}
