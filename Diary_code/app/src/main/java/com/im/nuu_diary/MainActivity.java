package com.im.nuu_diary;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    
    Toolbar toolbar;
    ArrayList<Record> listRecord=new ArrayList<>();
    private SQLite sqLite;
    private SQLiteDatabase db;
    ViewAdapter viewAdapter;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        listView=(ListView)findViewById(R.id.LISTrecord);
        setSupportActionBar(toolbar);

        // Menu item click 的監聽事件一樣要設定在 setSupportActionBar 才有作用
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);

        // 建立資料庫物件
        sqLite=new SQLite(MainActivity.this);
        db = sqLite.getWritableDatabase(); // 開啟資料庫
        sqLite.toNewTable("record");

        listRecord=sqLite.isHaveData();

        viewAdapter=new ViewAdapter(MainActivity.this,listRecord);
        listView.setAdapter(viewAdapter);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewAdapter.notifyDataSetChanged();
                Log.e("update","update");
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int rid=listRecord.get(position).getId();
                String title=listRecord.get(position).getTitle();
                String content=listRecord.get(position).getContent();
                String datetime=listRecord.get(position).getDatetime();
                int mood=listRecord.get(position).getMood();

                Intent intent=new Intent(MainActivity.this,Show_Record.class);
                //new一個Bundle物件，並將要傳遞的資料傳入
                Bundle bundle = new Bundle();
                bundle.putInt("id",rid);
                bundle.putString("title",title);//傳遞Double
                bundle.putString("content",content);
                bundle.putString("datetime",datetime);
                bundle.putInt("mood",mood);

                //將Bundle物件傳給intent
                intent.putExtras(bundle);

                //切換Activity
                startActivity(intent);

            }
        });



    }
    private Toolbar.OnMenuItemClickListener onMenuItemClickListener= new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            Toast.makeText(MainActivity.this, "新增一事件", Toast.LENGTH_SHORT).show();
            Log.e("跳頁新增","跳頁新增");
            Intent intent=new Intent();
            intent.setClass(MainActivity.this,new_record.class);
            startActivity(intent);
            return true;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 為了讓 Toolbar 的 Menu 有作用，這邊的程式不可以拿掉
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
