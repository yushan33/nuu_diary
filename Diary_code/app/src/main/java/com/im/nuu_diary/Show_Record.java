package com.im.nuu_diary;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Show_Record extends AppCompatActivity {
    Toolbar toolbar;
    private SQLite sqLite;
    private SQLiteDatabase db;
    TextView tvTitle,tvContent,tvDatetime;
    ImageView ivMood;
    int[] img={R.mipmap.laugh,R.mipmap.smail,R.mipmap.angry,R.mipmap.sad,R.mipmap.worry,R.mipmap.dead};

    String title,content,datetime;
    int id,mood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show__record);
        findview();
        toolbar=(Toolbar)findViewById(R.id.toolbar3);

        toolbar.setTitle("Show");

        setSupportActionBar(toolbar);

        // Menu item click 的監聽事件一樣要設定在 setSupportActionBar 才有作用
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);

        // 建立資料庫物件
        sqLite=new SQLite(Show_Record.this);
        db = sqLite.getWritableDatabase(); // 開啟資料庫
        sqLite.toNewTable("record");

        //取得傳值物件
        Bundle bundle = getIntent().getExtras();
        id=bundle.getInt("id") ;
        title=bundle.getString("title");
        mood=bundle.getInt("mood");
        content=bundle.getString("content");
        datetime=bundle.getString("datetime");

        //設置內容
        tvTitle.setText(title);
        tvContent.setText(content);
        tvDatetime.setText(datetime);
        ivMood.setImageResource(img[mood]);

    }

    private Toolbar.OnMenuItemClickListener onMenuItemClickListener= new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            switch (menuItem.getItemId()){
                case R.id.edit:
                    Intent intent_edit=new Intent(Show_Record.this,editRecord.class);
                    //new一個Bundle物件，並將要傳遞的資料傳入
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",id);
                    bundle.putString("title",title);//傳遞Double
                    bundle.putString("content",content);
                    bundle.putString("datetime",datetime);
                    bundle.putInt("mood",mood);

                    //將Bundle物件傳給intent
                    intent_edit.putExtras(bundle);
                    startActivity(intent_edit);
                    break;
                case R.id.delete:
                    boolean de=sqLite.delete(id);
                    Log.e("delete",Boolean.toString(de));
                    if(de==true){
                        Toast.makeText(Show_Record.this,"Delete success",Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(Show_Record.this,"Delete fail",Toast.LENGTH_LONG).show();
                    }
                    Intent intent_back=new Intent(Show_Record.this,MainActivity.class);
                    startActivity(intent_back);

                    break;
            }




            return true;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 為了讓 Toolbar 的 Menu 有作用，這邊的程式不可以拿掉
        getMenuInflater().inflate(R.menu.edit, menu);
        return true;
    }

    public void findview(){
        tvTitle=(TextView)findViewById(R.id.TVtitle);
        tvContent=(TextView)findViewById(R.id.TVcontent);
        tvDatetime=(TextView)findViewById(R.id.TVdatetime);
        ivMood=(ImageView)findViewById(R.id.IVmood);
        tvContent.setMovementMethod(ScrollingMovementMethod.getInstance());
    }
}
