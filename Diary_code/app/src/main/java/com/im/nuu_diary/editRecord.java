package com.im.nuu_diary;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;

public class editRecord extends AppCompatActivity {
    Toolbar toolbar;
    EditText edTitle,edContent;
    ImageButton ib1,ib2,ib3,ib4,ib5,ib6;
    Toolbar toolbar4;
    //SQLite sqLite=new SQLite(new_record.this);
    private SQLite sqLite;
    private SQLiteDatabase db;
    ImageButton[] ibArr={ib1,ib2,ib3,ib4,ib5,ib6};
    int[] imgButton={R.id.imageButton,R.id.imageButton2,R.id.imageButton3,R.id.imageButton4,R.id.imageButton5,R.id.imageButton6};
    String title,content,datetime;
    String[] st={"laugh","smile","angry","cry","worry","I want to dead"};
    int id,mood;
    int img=-1;
    int imgfor=-1;
    View viewF,viewN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_record);

        findview();

        // 建立資料庫物件
        sqLite=new SQLite(editRecord.this);
        db = sqLite.getWritableDatabase(); // 開啟資料庫
        sqLite.toNewTable("record");

        toolbar.setTitle("Edit");
        setSupportActionBar(toolbar);

        // Menu item click 的監聽事件一樣要設定在 setSupportActionBar 才有作用
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);

        //取得傳值物件
        Bundle bundle = getIntent().getExtras();
        id=bundle.getInt("id") ;
        title=bundle.getString("title");
        mood=bundle.getInt("mood");
        content=bundle.getString("content");
        datetime=bundle.getString("datetime");

        edTitle.setText(title);
        edContent.setText(content);



    }

    public void findview(){
        toolbar=(Toolbar)findViewById(R.id.toolbar2);
        edTitle=(EditText)findViewById(R.id.EDtitle);
        edContent=(EditText)findViewById(R.id.EDcontent);
        edContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        ib1=(ImageButton)findViewById(R.id.imageButton);
        ib2=(ImageButton)findViewById(R.id.imageButton2);
        ib3=(ImageButton)findViewById(R.id.imageButton3);
        ib4=(ImageButton)findViewById(R.id.imageButton4);
        ib5=(ImageButton)findViewById(R.id.imageButton5);
        ib6=(ImageButton)findViewById(R.id.imageButton6);

    }

    private Toolbar.OnMenuItemClickListener onMenuItemClickListener= new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            if(!edTitle.getText().toString().equals("") &&!edContent.getText().toString().equals("")&&img!=-1){
                title=edTitle.getText().toString();
                content=edContent.getText().toString();

                SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = sDateFormat.format(new java.util.Date());
                datetime=date;
                Log.e("time1111111111",datetime);

                if(sqLite.update(id,title,img,content,datetime)){
                    Toast.makeText(editRecord.this,"update success",Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(editRecord.this,MainActivity.class);
                    startActivity(intent);
                    System.exit(0);
                }else{
                    Toast.makeText(editRecord.this,"update fail",Toast.LENGTH_LONG).show();
                }


                System.exit(0);

            }else{
                Toast.makeText(editRecord.this,"could not null! ",Toast.LENGTH_LONG).show();
            }



            return true;
        }
    };

    public void ibClick(View v){


        for(int i=0;i<6;i++){
            if(v.getId()==imgButton[i]){
                viewF=viewN;
                img=i;
                viewN=v;
                v.getBackground().setAlpha(20);
            }
        }
        if(viewF!=null){
            viewF.getBackground().setAlpha(255);
        }

        Toast.makeText(editRecord.this,"You choose「"+st[img]+"」",Toast.LENGTH_LONG).show();
        Log.e("img2222",Integer.toString(img));
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 為了讓 Toolbar 的 Menu 有作用，這邊的程式不可以拿掉
        getMenuInflater().inflate(R.menu.insert, menu);
        return true;
    }
}
