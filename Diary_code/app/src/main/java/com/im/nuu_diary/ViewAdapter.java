package com.im.nuu_diary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ViewAdapter extends BaseAdapter {

    List<Record> listRe=new ArrayList<>();
    LayoutInflater inflater;

    static class ViewHolder{
        LinearLayout rlBorder;
        TextView TVtitle;
        TextView TVdate;
        ImageView IVmood;
    }

    //初始化
    public ViewAdapter(Context context, List<Record> listRe){
        this.listRe=listRe;
        inflater = LayoutInflater.from(context);
    }

    //取得數量
    @Override
    public int getCount() {
        return listRe.size();
    }

    //取得項目
    @Override
    public Object getItem(int position) {
        return listRe.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listRe.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView=inflater.inflate(R.layout.style_listview,null);
            holder.TVtitle=(TextView)convertView.findViewById(R.id.TVtitle);
            holder.TVdate=(TextView)convertView.findViewById(R.id.TVdate);
            holder.IVmood=(ImageView)convertView.findViewById(R.id.IVmood);
            holder.rlBorder=(LinearLayout)convertView.findViewById(R.id.LLre);
            convertView.setTag(holder);
        }
        else{
            holder=(ViewHolder)convertView.getTag();
        }

        int[] img={R.mipmap.laugh,R.mipmap.smail,R.mipmap.angry,R.mipmap.sad,R.mipmap.worry,R.mipmap.dead};

        holder.TVtitle.setText(listRe.get(position).getTitle());
        holder.TVdate.setText(listRe.get(position).getDatetime());

        holder.IVmood.setImageResource(img[listRe.get(position).getMood()]);

//        switch (listRe.get(position).getMood()){
//            case 0:{
//                holder.IVmood.setImageResource(R.mipmap.laugh);
//                break;
//            }
//            case 1:{
//                holder.IVmood.setImageResource(R.mipmap.smail);
//                break;
//            }
//            case 2:{
//                holder.IVmood.setImageResource(R.mipmap.angry);
//                break;
//            }
//            case 3:{
//                holder.IVmood.setImageResource(R.mipmap.sad);
//                break;
//            }
//            case 4:{
//                holder.IVmood.setImageResource(R.mipmap.worry);
//                break;
//            }
//            case 5:{
//                holder.IVmood.setImageResource(R.mipmap.dead);
//                break;
//            }
//
//        }

        return convertView;

    }


}
